<?php
/*
Template Name: Home
*/

if ( ! defined( 'ABSPATH' ) ) { exit; }
get_header();
?>

<div id="konkurs" class="white-popup mfp-hide">
    <div class="grid container contest-ended">
        <h1>KONKURS ZAKOŃCZYŁ SIĘ 22 PAŹDZIERNIKA.</h1>

        <div id="step1" class="tabcontent content-one">
            <div class="current-step">1</div>
            <div class="content">
                <h2></h2>
            </div>
        </div>

        <div id="step2" class="tabcontent content-two">
            <div class="current-step">2</div>
            <h2>Napisz dlaczego to właśnie bez niej nie wyobrażasz sobie jesiennej garderoby:</h2>
            <div class="content">
                <div class="image-container">
                    <img id="contest-img-2" src="" />
                    <a class="go-prev-step" href="#">Zmień wybór</a>
                </div>
                <div class="content-container">
                    <div class="textarea-wrapper">
                        <div class="textarea-outer">
                            <textarea id="contest-answer" rows="14" name="answer" maxlength="500" placeholder="Odpowiedź konkursowa"></textarea>
                        </div>
                        <div class="texatarea-chars">Pozostało <span id="answer-chars">500</span> znaków</div>
                    </div>
                    <button class="go-next-step btn btn__black">Przejdź dalej</button>
                </div>
            </div>    
        </div>

        <div id="step3" class="tabcontent">
            <div class="current-step">3</div>
            <div>
                <input type="text" name="fname" placeholder="imię" />
            </div>
            <div>
                <input type="text" name="lname" placeholder="nazwisko" />
            </div>
            <div>
                <input type="email" name="email" placeholder="email" />
            </div>
            <div>
                <input type="text" name="club" placeholder="numer karty KLUBU ORSAY">
            </div>
            <div class="agrees">
                <div class="main-agree">
                    <label>
                        <input type="checkbox" name="check1" value="0">
                        <span class="state"></span>   
                        <div class="label">Chcę zostać Ambasadorką Klubu Modnych Kobiet ORSAY</div>   
                    </label>
                </div>

                <div id="checkbox2">
                    <label>   
                        <input type="checkbox" name="check2" value="0">
                        <span class="state"></span>   
                        <span class="label">    
                        Przeczytałam i akceptuję regulamin konkursu „Zostań Ambasadorką” z uwzględnieniem informacji o ochronie i przetwarzaniu danych osobowych.</span>
                    </label>
                </div>

                <div>
                    <label>
                        <input type="checkbox" name="check3" value="0">
                        <span class="state"></span>   
                        <div class="label">
                        Przeczytałam i akceptuję regulamin konkursu „20 lat ORSAY”...<span class="agree-tooltip" title="Przeczytałam i akceptuję regulamin konkursu „20 lat ORSAY” z uwzględnieniem informacji o ochronie i przetwarzaniu danych osobowych.">Dalej</span></div>
                    </label>
                </div>

                <div>
                    <label>
                        <input type="checkbox" name="check4" value="0">
                        <span class="state"></span>   
                        <div class="label">Wyrażam zgodę na otrzymywanie informacji handlowych i materiałów promocyjnych oraz przesyłanie Newslettera przez ORSAY GmbH...<span class="agree-tooltip" title="Wyrażam zgodę na otrzymywanie informacji handlowych i materiałów promocyjnych oraz przesyłanie Newslettera przez ORSAY GmbH, z siedzibą w Willstätt, Niemcy, organ rejestracyjny Amtsgericht Freiburg HRB 370336, jak i Spółkę córkę „Ordipol” Sp. z o.o., z siedzibą w Bielanach Wrocławskich, ul. Logistyczna 1, NIP 677-200-16-69, REGON 35135919 na podany adres e-mail.">Dalej</span></div>
                    </label>
                </div>
            </div>    
            <button class="go-next-step btn btn__black">potwierdzam udział</button>
        </div>

        <div id="step4" class="tabcontent contest">
            <div class="current-step">4</div>
            <h3 class="contest__header margin-bottom-40">Dziękujemy za udział<br/> w konkursie!</h3>
            <p>
                O wynikach zostaniesz poinformowana poprzez maila.
            </p>    
            <div class="thanks-box">  
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/ebook.svg" alt="">
                </div>
                <div class="content-box">
                    <p>
                    Już teraz mamy dla Ciebie nagrodę gwarantowaną - <span class="big">PORADNIK MODNEJ KOBIETY</span> sygnowany przez Anię Starmach<br/> <a class="download-guide btn" href="#">Pobierz poradnik</a>
                    </p>    
                </div>
            </div>
            <div class="thanks-box">  
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/bon_20.svg" alt="">
                </div>
                <div class="content-box">
                    <p>
                    Cieszymy się, że razem z nami świętujesz 20 lat ORSAY w Polsce.
                    Będzie nam bardzo miło, jeśli dołączysz do Klubu Modnych Kobiet ORSAY.<br><br>
                    Czeka na Ciebie bon powitalny o wartości <span class="big">20 PLN</span> na zakupy w ORSAY oraz wiele innych przywilejów.<br>
                    <a href="http://20lat.orsay.com/#modnekobiety" id="ambasadorki-konkurs-link" class="btn">dowiedz się więcej</a>
                </div>
            </div> 
            <div class="thanks-box">  
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/bony.svg" alt="">
                </div>
                <div class="content-box">
                    <p>
                        Koniecznie sparwdź także naszą nową kolekcję i nie zapomnij dodać Karty Klubu ORSAY do swojego koszyka - <a href="http://www.orsay.com/pl-pl/nowosci.html?nsctrid=5555555117&utm_source=contest&utm_medium=referral&utm_campaign=20-years-pl&utm_content=contest" target='_blank' class="btn">sklep online</a>
                    </p>
                </div>
            </div>    
            <button class="contest__btn btn btn__black">WRÓĆ NA STRONĘ</button>
        </div>
            
            
        <div id="step5" class="tabcontent contest">
            <div class="current-step">5</div>
            <h3 class="contest__header margin-bottom-40">Dziękujemy za udział<br/> w konkursie!</h3>
            <p>
            O wynikach zostaniesz poinformowana poprzez maila.<br><br>
            <div class="thanks-box">  
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/ebook.svg" alt="">
                </div>
                <div class="content-box">
                    <p>
                        Już teraz mamy dla Ciebie nagrodę gwarantowaną - PORADNIK MODNEJ KOBIETY sygnowany przez Anię Starmach<br/> <a class="download-guide btn" href="#">Pobierz poradnik</a>
                    </p>
                </div>
            </div>    
            <div class="thanks-box">  
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/bony.svg" alt="">
                </div>
                <div class="content-box">
                    <p>
                        Cieszymy się, że razem z nami świętujesz 20 lat ORSAY w Polsce. <br/>
                        Koniecznie sprawdź co przygotowaliśmy dla Ciebie na jesień <a href="http://www.orsay.com/pl-pl/nowosci.html?nsctrid=5555555117&utm_source=contest&utm_medium=referral&utm_campaign=20-years-pl&utm_content=contest" target="_blank" class="btn">NOWA KOLEKCJA</a>
                    </p>
                </div>    
            </div>
            Bądź na bieżąco z nowościami - śledź nas na <a href="https://www.facebook.com/orsaypl/" class="facebook-inline"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/facebook_icon.svg" alt=""></a> Facebook'u i <a target="_blank" href="https://www.instagram.com/orsay/">Instagramie</a>.
            </p>
            <button class="contest__btn btn btn__black">WRÓĆ NA STRONĘ</button>
            </div>

            <div id="step6" class="tabcontent contest">
            <div class="current-step">6</div>
            <h4 class="contest__header margin-bottom-40">Potwierdź swój udział w konkursie, klikając w link wysyłany na Twój adres e-mailowy.</h4>
            </div>
        </div>

    </div>    
</div>

<div id="page-content">
    <div class="grid container"></div>
    <div class="container">
        <section class="my-choice home-section">
            <div class="row" id="mobile-reverse">
                <div class="col col-50">   
                <div id="stylization-sliders">
                </div>    
            </div>
                <div class="col col-50 products-wrapper">
                    <div id="datesSlider">
                        <div class="item">09.10 - 22.10</div>
                        <div class="item">09.10 - 22.10</div>
                        <div class="item">25.09 - 08.10</div>
                        <div class="item">25.09 - 08.10</div>
                        
                        <div class="item">11.09 - 24.09</div>
                        <div class="item">11.09 - 24.09</div>
                        <div class="item">28.08 - 10.09</div>
                        <div class="item">28.08 - 10.09</div>
                    </div>
                    <div id="product-slider" class="product-slider">
                        <div class="item pre-three">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-ania-3.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item pre-four">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-ania-4.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item pre-one">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-ania-1.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item pre-two">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-ania-2.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item one">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-02.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item two">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-03.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item three">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                        <div class="item four">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-slider-04.jpg" alt="">
                            <div class="icons">
                                <div class="icon-item"></div>
                                <div class="icon-item"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </section>
        <section class="margin-bottom-30 margin-top-70 section-image-left home-section" id="ambasadors-section">
            <div id="waypoint-1">
                <header class="section-header">
                    <h3 id="ambasadorki" class="block-450">
                        zostań ambasadorką klubu modnych kobiet orsay
                    </h3>
                </header>
                <div class="row margin-bottom-95" id="unfix">
                    <div class="col col-25">
                        <div id="fixed-box">
                            <div class="feature-img-container">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/stage4/konkurs.jpg" alt="">
                                <div class="feature-img-label-container">
                                    <div class="feature-img-label">konkurs</div>
                                </div>    
                            </div>
                        </div>    
                    </div>
                    <div class="col col-75 padding-left-160 waypoint-1-intro">
                        <p>
                            Wystarczy spełnić dwa warunki - należeć do Klubu ORSAY oraz wziąć udział w naszym konkursie.
                        </p>
                        <p>
                            Zadanie jest proste: wybierz Twoją ulubioną rzecz z kolekcji i napisz dlaczego to właśnie bez niej nie wyobrażasz sobie jesiennej garderoby. 
                        </p>
                        <p>
                            Na Ambasadorkę czekają wyjątkowe nagrody:
                        </p>
                        <div class="padding-left-40" id="waypoint-1-1">
                            <ul class="features-list">
                                <li>
                                    <div class="img-box">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/bony.svg" alt="">
                                    </div>
                                    <div class="content-box">
                                        <div class="content-inner">
                                            bony na zakupy w ORSAY o łącznej wartości 6000 PLN
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="img-box">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/sesja.svg" alt="">
                                    </div>
                                    <div class="content-box">
                                        wyjątkowa sesja zdjęciowa
                                    </div>
                                </li>
                                <li>
                                    <div class="img-box">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/kolacja.svg" alt="">
                                    </div>
                                    <div class="content-box">
                                        kolacja z pozostałymi Ambasadorkami:<br>
                                        Jessicą Mercedes i Anią Starmach
                                    </div>
                                </li>
                                <li>
                                    <div class="img-box">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/udzial.svg" alt="">
                                    </div>
                                    <div class="content-box">
                                        udział w wydarzeniach specjalnych<br>
                                        eventy klubowe, spotkania ze stylistami, redaktorami modowymi i blogerkami
                                    </div>
                                </li>
                                <li>
                                    <div class="img-box">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/reprezentowanie.svg" alt="">
                                    </div>
                                    <div class="content-box">
                                        reprezentowanie Klubu Modnych Kobiet ORSAY
                                    </div>
                                </li>
                            </ul>
                            <div class="center-mobile">
                                <a href="#konkurs" class="btn open-popup-link">weź udział w konkursie</a>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>    
            <section class="section-image-right flex-mobile-section">
                <div class="row" id="waypoint-2">
                    <div class="col col-50 col-offset-25 padding-left-80 padding-right-50">
                        <header class="sub-section-header">
                            <h4 id="contest" class="block-400 ">nie jesteś zainteresowana mianem ambasadorki klubu modnych kobiet?</h4>
                        </header>
                        <p>
                            Ten konkurs jest również dla Ciebie! Co 2 tygodnie masz szansę wygrać inne, atrakcyjne nagrody:
                        </p>
                        <ul class="features-list">
                            <li>
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/bon_200.svg" alt="">
                                </div>
                                <div class="content-box">
                                    5x bon 200 PLN na zakupy w ORSAY
                                </div>
                            </li>
                            <li>
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/zapach.svg" alt="">
                                </div>
                                <div class="content-box">
                                    20x zestaw modnych akcesoriów: torebka + zapach L’OR DE SAY
                                </div>
                            </li>
                            <li>
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/ebook.svg" alt="">
                                </div>
                                <div class="content-box">
                                    Nagroda gwarantowana: poradnik Modnej Kobiety, sygnowany przez Jessicę Mercedes lub Anię Starmach
                                </div>
                            </li>
                        </ul>
                        <div class="padding-left-75 center-mobile padding-left-120 reset-mobile-left-padding">
                            <a href="#konkurs" class="btn open-popup-link">weź udział w konkursie</a>
                        </div>
                    </div>
                    <div class="col col-25 col-shrink-mobile">
                        <div class="feature-img-container reverse" id="fixed-box-2">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/stage4/ambasadorka.jpg" alt="">
                        </div>
                    </div>
                </div>    
            </section>
        </section>
        <div id="modnekobiety"></div>
        <section class="margin-bottom-140 section-image-left home-section" id="fixed-box-limit">
            <div class="row padding-top-70" id="waypoint-3">
                <div class="col col-25">
                    <div class="feature-img-container" id="fixed-box-3">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/stage4/modne_kobiety.jpg" alt="">
                    </div>
                </div>
                <div class="col col-50 padding-left-80 padding-right-50">
                    <header class="section-header">
                        <h3>Bycie członkiem klubu modnych kobiet orsay to wyjątkowe korzyści:</h3>
                    </header>
                    <ul class="features-list padding-left-40">
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/pkt.svg" alt="">
                            </div>
                            <div class="content-box">
                                Punkty za zakupy do wymiany na cenne BONY
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/bon_20.svg" alt="">
                            </div>
                            <div class="content-box">
                                BON POWITALNY o wartości 20 PLN i dodatkowe 800 punktów klubowych
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/niespodzianka.svg" alt="">
                            </div>
                            <div class="content-box">
                                Niespodzianka urodzinowa
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/rabaty.svg" alt="">
                            </div>
                            <div class="content-box">
                                Ekskluzywne oferty i rabaty
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/krawiec.svg" alt="">
                            </div>
                            <div class="content-box">
                                Zniżka na usługi krawieckie i inne przywileje klubowe
                            </div>
                        </li>
                    </ul>    
                </div>
            </div>
        </section>
        <section class="padding-bottom-105 home-section" id="fixed-box-3-limit">
            <div class="row margin-bottom-90" id="waypoint-4">
                <div class="col col-25">
                    <header class="section-header" id="fixed-box-4">
                        <h3 class="mobile-heaer-marign">ekskluzywne eventy urodzinowe w salonach</h3>
                    </header>
                </div>
                <div class="col col-75 padding-left-80">
                    <p>
                        20 lat ORSAY w Polsce to 20 okazji do wspólnego świętowania! Spotkajmy się podczas wyjątkowych eventów w salonach ORSAY. Przygotowaliśmy dla Was specjalne, urodzinowe atrakcje:
                    </p>
                    <ul class="features-list padding-left-80">
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/rabaty.svg" alt="">
                            </div>
                            <div class="content-box">
                                Rabat 20% na zakupy
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/porady.svg" alt="">
                            </div>
                            <div class="content-box">
                                Porady stylistów
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/prezent.svg" alt="">
                            </div>
                            <div class="content-box">
                                Upominki dla Ciebie i Twojej przyjaciółki
                            </div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icons/niespodzianka.svg" alt="">
                            </div>
                            <div class="content-box">
                                Inne, stylowe niespodzianki
                            </div>
                        </li>
                    </ul>
                    <p>
                        Śledźcie szczegóły w wydarzeniach na naszym profilu 
                        <a target="_blank" href="https://www.facebook.com/orsaypl/" class="facebook-inline"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/facebook_icon.svg" alt=""></a> Facebook oraz w urodzinowych salonach.
                    </p>
                    <p>
                        Do zobaczenia!
                    </p>
                </div>
            </div>
            <div id="fixed-box-4-limit">
                <div class="row margin-bottom-70 margin-bottom-mobile-0" id="waypoint-5">
                    <div class="col col-50">
                        <div class="map-wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/map.svg" alt="">
                            <div class="pins">
                                <div class="pin pin__large warszawa" data-tooltip-content="#tooltip_warszawa">
                                    <div class="label">5</div>
                                    <div id="tooltip_warszawa" class="tooltip-map">
                                        <span class="city">Warszawa</span>
                                        <span>14 Września 2017</span>
                                        <span>23 Września 2017</span>
                                        <span>30 Września 2017</span>
                                        <span>7 Października 2017</span>
                                        <span>20 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin poznan" data-tooltip-content="#tooltip_poznan">
                                    <div id="tooltip_poznan" class="tooltip-map">
                                        <span class="city">Poznań</span>
                                        <span>21 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin lodz" data-tooltip-content="#tooltip_lodz">
                                    <div class="label">2</div>
                                    <div id="tooltip_lodz" class="tooltip-map">
                                        <span class="city">Łódź</span>
                                        <span>30 Września 2017</span>
                                        <span>21 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin zielona-gora" data-tooltip-content="#tooltip_zielona_gora">
                                    <div id="tooltip_zielona_gora" class="tooltip-map">
                                        <span class="city">Zielona Góra</span>
                                        <span>21 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin szczecin" data-tooltip-content="#tooltip_szczecin">
                                    <div id="tooltip_szczecin" class="tooltip-map">
                                        <span class="city">Szczecin</span>
                                        <span>7 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin rumia" data-tooltip-content="#tooltip_rumia">
                                    <div id="tooltip_rumia" class="tooltip-map">
                                        <span class="city">Rumia</span>
                                        <span>14 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin gdansk" data-tooltip-content="#tooltip_gdansk">
                                    <div id="tooltip_gdansk" class="tooltip-map">
                                        <span class="city">Gdańsk</span>
                                        <span>30 Września 2017</span>
                                    </div>
                                </div>
                                <div class="pin wroclaw" data-tooltip-content="#tooltip_wroclaw">
                                    <div id="tooltip_wroclaw" class="tooltip-map">
                                        <span class="city">Wrocław</span>
                                        <span>30 Września 2017</span>
                                    </div>
                                </div>
                                <div class="pin legnica" data-tooltip-content="#tooltip_legnica">
                                    <div id="tooltip_legnica" class="tooltip-map">
                                        <span class="city">Legnica</span>
                                        <span>21 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin czeladz" data-tooltip-content="#tooltip_czeladz">
                                    <div id="tooltip_czeladz" class="tooltip-map">
                                        <span class="city">Czeladź</span>
                                        <span>14 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin katowice" data-tooltip-content="#tooltip_katowice">
                                    <div id="tooltip_katowice" class="tooltip-map">
                                        <span class="city">Katowice</span>
                                        <span>7 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin krakow" data-tooltip-content="#tooltip_krakow">
                                    <div id="tooltip_krakow" class="tooltip-map">
                                        <span class="city">Kraków</span>
                                        <span>30 Września 2017</span>
                                    </div>
                                </div>
                                <div class="pin bielsko-biala" data-tooltip-content="#tooltip_bielsko-biala">
                                    <div id="tooltip_bielsko-biala" class="tooltip-map">
                                        <span class="city">Bielsko-Biała</span>
                                        <span>30 Września 2017</span>
                                    </div>
                                </div>
                                <div class="pin mikolow" data-tooltip-content="#tooltip_mikolow">
                                    <div id="tooltip_mikolow" class="tooltip-map">
                                        <span class="city">Mikołów</span>
                                        <span>14 Października 2017</span>
                                    </div>
                                </div>
                                <div class="pin kielce" data-tooltip-content="#tooltip_kielce">
                                    <div id="tooltip_kielce" class="tooltip-map">
                                        <span class="city">Kielce</span>
                                        <span>14 Października 2017</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-50">
                        <table class="events-table">
                            <tr>
                                <th>miasto</th>
                                <th>sklep</th>
                                <th>data</th>
                                <th>wydarzenie fb</th>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="events-table-wrapper" id="events">
                                        <table class="events-table-inner">
                                        <tr>        
                                            <td data-th="Miasto">Warszawa</td>
                                            <td data-th="Sklep">ul. Chmielna</td>
                                            <td data-th="Data">14 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://web.facebook.com/events/346771642414970/?acontext=%7B%22ref%22%3A%223%22%2C%22ref_newsfeed_story_type%22%3A%22regular%22%2C%22feed_story_type%22%3A%22117%22%2C%22action_history%22%3A%22null%22%7D&_rdc=1&_rdr" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Warszawa</td>
                                            <td data-th="Sklep">Arkadia</td>
                                            <td data-th="Data">23 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/668113470064796/" target="_blank">przejdź</a></td>
                                        </tr>    
                                        <tr>        
                                            <td data-th="Miasto">Warszawa</td>
                                            <td data-th="Sklep">G. Mokotów</td>
                                            <td data-th="Data">30 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1871744852840023/" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Bielsko-Biała</td>
                                            <td data-th="Sklep">C. H. Auchan</td>
                                            <td data-th="Data">30 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1871744852840023/" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Łódź</td>
                                            <td data-th="Sklep">G. Łódzka</td>
                                            <td data-th="Data">30 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1871744852840023/" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Gdańsk</td>
                                            <td data-th="Sklep">G. Madison</td>
                                            <td data-th="Data">30 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1871744852840023/" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Wrocław</td>
                                            <td data-th="Sklep">G. Dominikańska</td>
                                            <td data-th="Data">30 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1871744852840023/" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Kraków</td>
                                            <td data-th="Sklep">G. Krakowska</td>
                                            <td data-th="Data">30 Wrz 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1871744852840023/" target="_blank">przejdź</a></td>
                                        </tr>    
                                        <tr>
                                            <td data-th="Miasto">Warszawa</td>
                                            <td data-th="Sklep">G. Wileńska</td>
                                            <td data-th="Data">6 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/472262629826970/" target="_blank">przejdź</a></td>
                                        </tr>    
                                        <tr>
                                            <td data-th="Miasto">Szczecin</td>
                                            <td data-th="Sklep">C.H. Gryf</td>
                                            <td data-th="Data">7 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/340631899680820/" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Katowice</td>
                                            <td data-th="Sklep">Silesia Park</td>
                                            <td data-th="Data">7 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/340631899680820/" target="_blank">przejdź</a></td>
                                        </tr>    
                                        <tr>
                                            <td data-th="Miasto">Rumia</td>
                                            <td data-th="Sklep">C.H. Auchan</td>
                                            <td data-th="Data">14 Paź 2017</td>
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1967701103505960/" target="_blank">przejdź</a></td>
                                            <!--<td data-th="Wydarzenie FB"><a href="#" target="_blank">przejdź</a></td>-->
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Czeladź</td>
                                            <td data-th="Sklep">C.H. M1</td>
                                            <td data-th="Data">14 Paź 2017</td>
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1967701103505960/" target="_blank">przejdź</a></td>
                                            <!--<td data-th="Wydarzenie FB"><a href="#" target="_blank">przejdź</a></td>-->
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Mikołów</td>
                                            <td data-th="Sklep">C.H. Auchan</td>
                                            <td data-th="Data">14 Paź 2017</td>
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1967701103505960/" target="_blank">przejdź</a></td>
                                            <!--<td data-th="Wydarzenie FB"><a href="#" target="_blank">przejdź</a></td>-->
                                        </tr>  
                                        <tr>
                                            <td data-th="Miasto">Kielce</td>
                                            <td data-th="Sklep">G. Echo</td>
                                            <td data-th="Data">14 Paź 2017</td>
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/1967701103505960/" target="_blank">przejdź</a></td>
                                            <!--<td data-th="Wydarzenie FB"><a href="#" target="_blank">przejdź</a></td>-->
                                        </tr>        
                                        <tr>
                                            <td data-th="Miasto">Warszawa</td>
                                            <td data-th="Sklep">Złote Tarasy</td>
                                            <td data-th="Data">20 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/2009686929268626/" target="_blank">przejdź</a></td>
                                        </tr>    
                                        <tr>
                                            <td data-th="Miasto">Poznań</td>
                                            <td data-th="Sklep">Posnania</td>
                                            <td data-th="Data">21 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/362398640871118" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Łódź</td>
                                            <td data-th="Sklep">Manufaktura</td>
                                            <td data-th="Data">21 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/362398640871118" target="_blank">przejdź</a></td>
                                        </tr>
                                        
                                        <tr>
                                            <td data-th="Miasto">Zielona Góra</td>
                                            <td data-th="Sklep">Focus Park</td>
                                            <td data-th="Data">21 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/362398640871118" target="_blank">przejdź</a></td>
                                        </tr>
                                        <tr>
                                            <td data-th="Miasto">Legnica</td>
                                            <td data-th="Sklep">G. Piastów</td>
                                            <td data-th="Data">21 Paź 2017</td>
                                            <!--<td data-th="Wydarzenie FB">Szczegóły wkrótce</td>-->
                                            <td data-th="Wydarzenie FB"><a href="https://www.facebook.com/events/362398640871118" target="_blank">przejdź</a></td>
                                        </tr>
                                    </table>
                                    </div>    
                                </td>
                            </td>
                        </table>
                    </div>
                </div>

                <div class="center">
                    <a href="#" id="gallery-btn" class="btn">zobacz jak świętujemy</a>
                </div>

                <div id="gallery">
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2400.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2406.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2408.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2481.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2494.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2518.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2524.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2541.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2550.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2709.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2712.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2844.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_2858.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_3010.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_3040.jpg"></a>
                    
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1787.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1766.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1735.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1630.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1573.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1499.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1472.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1457.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1411.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1392.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1285.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1276.jpg"></a>
                    <a href="https://20lat.orsay.com/wp-content/uploads/2017/09/DSC_1229.jpg"></a>
                    
                    
                </div>
                
            </div>
        </section>
    </div>
</div>    
<?php get_footer(); ?>
