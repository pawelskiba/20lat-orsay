module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json"),

        uglify: {
            options: {
                mangle: {
                    except: ['jQuery', '$']
                }
            },
            my_target: {
                files: {
                    'assets/js/vendor.min.js': [
                        'assets/js/includes/jquery-2.2.4.js',
                        'assets/js/includes/magnific-popup.js',
                        'assets/js/includes/limiter.js',
                        'assets/js/includes/plugins.js',
                        'assets/js/includes/pxloader-images.js',
                        'assets/js/includes/tooltipster.bundle.js',
                        'assets/js/includes/mobile-detect.js',
                        'assets/js/includes/perfect-scrollbar.jquery.js',
                        'assets/js/includes/jquery-scrolltofixed.js',
                        'assets/js/includes/slick.js',
                        'assets/js/includes/jquery.flipster.min.js',
                        'assets/js/includes/jquery.waypoints.min.js'
                    ]
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');

};
