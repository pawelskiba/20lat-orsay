function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if(results == null)
    return "";
    else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
} 

(function ($) {

    //-- delay function for resizing
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        }
    })();

    var openTab = function(tab_id){
        var i,
            tabcontent = $('.tabcontent'),
            tablinks = $('.tablinks');

        for (i = 0; i < tabcontent.length; i++) {
            tabcontent.each(function(){
                $(this).css('display', 'none');
            });
        }
        for (i = 0; i < tablinks.length; i++) {
            tablinks.each(function(){
                $(this).toggleClass('active');
            });
        }
        $('#step'+tab_id).css('display', 'block');
    }
    
    function setPopupGallery() {
        $('#gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Ładowanie...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            }
        });

    }

    //-- on document ready
    $(document).ready(function () {
        $.ajaxSetup({ cache: false });

        var hide_tabs = false;

        
        $('#gallery-btn').on('click', function (e) {
            e.preventDefault()
            setPopupGallery()
            $('#gallery').magnificPopup('open');
        });
        
        
        //if get param
        if (getParameterByName('pdf').length > 0 && getParameterByName('ambasador').length > 0) {

            //tabid
            var tabid = 0;

            hide_tabs = true;

            if(getParameterByName('ambasador') == 'TAK'){
                tabid = 5;
            }else{
                tabid = 4;
            }

            $('.open-popup-link').magnificPopup({
                type:'inline',
                midClick: true,
                callbacks: {
                    open: function() {
                        openTab(tabid);
                        if( hide_tabs == true ){
                            $('.steps').css('display', 'none');
                            $('#konkurs h1').css('display', 'none');

                            $('a.download-guide').addClass('can_download').attr('href', fr.theme_url+'/download.php?key='+getParameterByName('pdf'));

                            $('.can_download').on('click', function(){
                                //ga code
                                ga('send', {
                                    hitType: 'event',
                                    eventCategory: 'Pobranie PDF',
                                    eventAction: 'click',
                                    eventLabel: 'Poradniak 20 lat'
                                });
                                $(this).removeClass('can_download');
                            });
                        }
                    },
                    close: function(){
                        if($("#contest-slider").hasClass("slick-initialized")){
                            contestSlider.slick('unslick');
                        }
                        window.location.href = fr.home_url;
                    }
                }
            }).trigger('click');

        }

        //-- scroll on click/tap
        $('.hash').on('click tochend', function(e) {

            e.preventDefault();

            var target = this.hash,
                $target = $(target);

            if( $target.length > 0 ){

                //scroll to
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top - 30
                }, 'slow', 'swing', function(){
                    window.location.hash = target;
                });
            }else{
                window.location = fr.home_url+'/'+target;
            }
        });
        
        var contestSlider;
        function buildSlider() {
            if(!($("#contest-slider").hasClass("slick-initialized"))){
               return $("#contest-slider").slick({
                    arrows: true,
                    infinite: false,
                    slidesToShow: 4,
                    prevArrow: '<i class="slick-prev"><img src="'+fr.theme_url+'/assets/img/svg/arrow_left.svg" alt=""></i>',
                    nextArrow: '<i class="slick-next"><img src="'+fr.theme_url+'/assets/img/svg/arrow_right.svg" alt=""></i>', 
                    responsive: [{

                        breakpoint: 1366,
                        settings: {
                            slidesToShow: 3,
                    }

                    }, {

                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 2,
                    }

                    },{

                        breakpoint: 991,
                        settings: {
                            slidesToShow: 4,
                    }

                    },{

                        breakpoint: 767,
                        settings: {
                            slidesToShow: 3,
                    }

                    },
                    {

                        breakpoint: 479,
                        settings: {
                            slidesToShow: 2,
                    }

                    },
                    {

                        breakpoint: 300,
                        settings: "unslick" // destroys slick

                    }]
                });
            }
        }

        // -- open popup on click
        $('.open-popup-link').magnificPopup({
            type:'inline',
            midClick: true,
            callbacks: {
                open: function() {
                    openTab(1);
                    var elem = $("#answer-chars");
                    if( elem.length == 1 ){
                        $("#contest-answer").limiter(500, elem);
                    };
                    $(".steps .item").removeClass("active done");
                    $(".steps .item:nth-child(1)").addClass("active"); 
                    
                    if(!($("#contest-slider").hasClass("slick-initialized"))){
                        contestSlider = buildSlider()
                    }  
                },
                close: function(){
                    if($("#contest-slider").hasClass("slick-initialized")){
                        contestSlider.slick('unslick');
                    }
                }
            }
        });
        
        
        $("#ambasadorki-konkurs-link").click(function(){
            $('.open-popup-link').magnificPopup('close');
        })
        

        if( window.location.hash === '#konkurs' ){
            $('.open-popup-link').trigger('click');
        }

        // -- show checkbox2 if check1 is checked
        $('input[name="check1"]').on('change', function(){
            var check2_wrapper = $('#checkbox2');
            if( $(this).is(':checked') ){
                check2_wrapper.fadeIn();
            }else{
                check2_wrapper.fadeOut();
            }
        });

        // -- disable check1 if check2 is checked
        $('input[name="check2"]').on('change', function(){
            if( $(this).is(':checked') ){
                $('input[name="check1"]').prop('disabled', true);
            }else{
                $('input[name="check1"]').prop('disabled', false);
            }
        });
        
        // -- agrees tooltips
        $('.agree-tooltip').tooltipster({
            side: 'right',
            trigger: 'click'
        });
        
        $('.pins .pin').tooltipster({
            triggerOpen: {
                tap: true,
                touchstart: true
            },
        });

        // -- custom close btn
        var magnificPopup = $.magnificPopup.instance;
        $('.contest__btn').on('click', function(e){
            e.preventDefault();
            magnificPopup.close();
        });

        // -- go next step
        $('.go-next-step').on('click', function(e){
            e.preventDefault();
            
            if($("#contest-slider").hasClass("slick-initialized")){
                contestSlider.slick('unslick');
            }

            var current_step = parseInt($(this).parents(".tabcontent").children('div.current-step').html()),
                tab_id,
                contest_img_input = $('#contest-img-input'),
                error = false,
                errorMsg = '';

            tab_id = current_step + 1;

            //step 1
            if( current_step == 1 ){
                var contest_img_src = $(this).attr('src');
                contest_img_input.val(contest_img_src);
                //validate if img is selected
                if( contest_img_input.val() == '' ){
                    error = true;
                }else{
                    //if no errors set img src to next step img
                    var contest_img_2 = $('#contest-img-2');
                    contest_img_2.attr('src', contest_img_input.val());
                    openTab(tab_id);
                }
                if ($(window).width() < 992) {
                    var scrollPos = $("#contest").offset().top - 10
                    $("html, body").animate({scrollTop: scrollPos}, 300)
                }
            }

            //step 2
            if( current_step == 2 ){
                //validate textarea
                var contest_textarea_count = $('#contest-answer').val().length,
                    answer_error = false;

                if( contest_textarea_count == 0 ){
                    error = true;
                    answer_error = true;
                    errorMsg = "To pole jest wymagane.";
                }else if( contest_textarea_count > 500 ){
                    error = true;
                    answer_error = true;
                    errorMsg = "Maksymalna ilość znaków to 500."; 
                }else if( contest_textarea_count < 20 ){
                    error = true;
                    answer_error = true;
                    errorMsg = "Minimalna ilość znaków to 20."; 
                }

                if( answer_error == true ){
                    var e_placement = $('.textarea-wrapper');
                    if( e_placement.next('.error').length == 0 ){
                        //insert error
                        $('<div id="answer-error" class="error">'+errorMsg+'</div>')
                            .css('display', 'none')
                            .insertAfter(e_placement)
                            .fadeIn();
                        //addclass
                        $('#contest-answer').addClass('error');
                    }else{
                        $('#textarea-error').html(errorMsg);
                    }
                }else{
                    //if no errors go next step
                    $(".steps .item").removeClass("active");
                    $(".steps .item:nth-child(1)").addClass("done");
                    $(".steps .item:nth-child(2)").addClass("active");
                    if ($(window).width() < 992) {
                        var scrollPos = $("#contest").offset().top - 10
                        $("html, body").animate({scrollTop: scrollPos}, 300)
                    }
                    openTab(tab_id);
                }
            }

            //step 3
            if( current_step == 3 ){
                //validate fname
                var fname= $('input[name="fname"]');
                if( fname.val() == '' ){
                    error = true;
                    errorMsg = "To pole jest wymagane.";
                    var e_placement = fname.parent();
                    if( e_placement.next('.error').length == 0 ){
                        //insert error
                        $('<div id="fname-error" class="error">'+errorMsg+'</div>')
                            .css('display', 'none')
                            .insertAfter(e_placement)
                            .fadeIn();
                        //addclass
                        fname.addClass('error');
                    }else{
                        $('#fname-error').html(errorMsg);
                    }
                }else{
                    if( $('#fname-error').length > 0 ){
                        $('#fname-error').fadeOut(function(){
                            $(this).remove();
                        });
                        fname.removeClass('error');
                    }
                }

                //validate lname
                var lname = $('input[name="lname"]');
                if( lname.val() == '' ){
                    error = true;
                    errorMsg = "To pole jest wymagane.";
                    var e_placement = lname.parent();
                    if( e_placement.next('.error').length == 0 ){
                        //insert error
                        $('<div id="lname-error" class="error">'+errorMsg+'</div>')
                            .css('display', 'none')
                            .insertAfter(e_placement)
                            .fadeIn();
                        //addclass
                        lname.addClass('error');
                    }else{
                        $('#lname-error').html(errorMsg);
                    }
                }else{
                    if( $('#lname-error').length > 0 ){
                        $('#lname-error').fadeOut(function(){
                            $(this).remove();
                        });
                        lname.removeClass('error');
                    }
                }

                //validate email
                var email = $('input[name="email"]'),
                    email_error = false;
                if( email.val() == '' ){
                    error = true;
                    email_error = true;
                    errorMsg = "To pole jest wymagane.";
                }else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val())){
                    error = true;
                    email_error = true;
                    errorMsg = "Niepoprawny adres email.";
                    
                }else if( $('#email-error').length > 0 ){
                    $('#email-error').fadeOut(function(){
                        $(this).remove();
                    });
                    email.removeClass('error');
                }

                if( email_error == true ){
                    var e_placement = email.parent();
                    if( e_placement.next('.error').length == 0 ){
                        //insert error
                        $('<div id="email-error" class="error">'+errorMsg+'</div>')
                            .css('display', 'none')
                            .insertAfter(e_placement)
                            .fadeIn();
                        //addclass
                        email.addClass('error');
                    }else{
                        $('#email-error').html(errorMsg);
                    }
                }

                //validate check1 & check2
                var check1 = $('input[name="check1"]'),
                    check2 = $('input[name="check2"]');
                if( check1.is(':checked') ){
                    if( ! check2.is(':checked') ){
                        error = true;
                        errorMsg = "To pole jest wymagane.";
                        var e_placement = check2.parent();
                        if( e_placement.next('.error').length == 0 ){
                            $('<div id="check2-error" class="error">'+errorMsg+'</div>')
                                .css('display', 'none')
                                .insertAfter(e_placement)
                                .fadeIn();
                        }else{
                            $('#check2-error').html(errorMsg);
                        }
                    }else{
                        check2.attr('value', 1);
                        if( $('#check2-error').length > 0 ){
                            $('#check2-error').fadeOut(function(){
                                $(this).remove();
                            });
                        }
                    }
                    
                    // go to step 6 if checked
                    tab_id += 1;
                }
                
                //validate check3
                var check3 = $('input[name="check3"]');
                if( ! check3.is(':checked') ){
                    error = true;
                    errorMsg = "To pole jest wymagane.";
                    var e_placement = check3.parent();
                    if( e_placement.next('.error').length == 0 ){
                        $('<div id="check3-error" class="error">'+errorMsg+'</div>')
                            .css('display', 'none')
                            .insertAfter(e_placement)
                            .fadeIn();
                    }else{
                        $('#check3-error').html(errorMsg);
                    }
                }else{
                    check3.attr('value', 1);
                    if( $('#check3-error').length > 0 ){
                        $('#check3-error').fadeOut(function(){
                            $(this).remove();
                        });
                    }
                }

                var check4 = $('input[name="check4"]');
                if( check4.is(':checked') ){
                    check4.attr('value', 1);
                }

                //if no error go to next step
                if( error === false ){
                    
                    $.ajax({
                        url:fr.ajax_url,
                        data: {
                            action: 'process_contest_form',
                            security: fr.ajax_nonce,
                            img_src: $('input[name="img_src"]').val(),
                            fname: fname.val(),
                            lname: lname.val(),
                            answer: $('#contest-answer').val(),
                            club: $('input[name="club"]').val(),
                            email: email.val(),
                            check2: check2.val(),
                            check3: check3.val(),
                            check4: check4.val(),
                        },
                        type: 'POST',
                        dataType: "json",
                        beforeSend: function(){
                        },
                        success: function(data){

                            if( data.errors.length === 0 && data.passed == true ){

                                window._adftrack.push({
                                    pm: 1002616,
                                    divider: encodeURIComponent('|'),
                                    pagename: encodeURIComponent('TYP konkurs Orsay')
                                });
                                $('body').append('<p style="margin:0;padding:0;border:0;"><img src="https://track.adform.net/Serving/TrackPoint/?pm=1002616&ADFPageName=TYP%20konkurs%20Orsay&ADFdivider=|" width="1" height="1" alt="" /></p>');
                                
                                //1
                                ttConversionOptions.push({
                                    type: 'lead',
                                    campaignID: '26620',
                                    productID: '39143',
                                    transactionID: data.client_id,
                                    email: data.client_email,
                                    descrMerchant: '',
                                    descrAffiliate: ''
                                });
                                $('body').append('<img src="//tl.tradetracker.net/?cid=26620&amp;pid=39143&amp;tid='+data.client_id+'&amp;data=&amp;eml='+data.client_email+'&amp;descrMerchant=&amp;descrAffiliate=&amp;event=lead" alt="" />');
                                (function(ttConversionOptions) {
                                    var campaignID = 'campaignID' in ttConversionOptions ? ttConversionOptions.campaignID : ('length' in ttConversionOptions && ttConversionOptions.length ? ttConversionOptions[0].campaignID : null);
                                    var tt = document.createElement('script'); tt.type = 'text/javascript'; tt.async = true; tt.src = '//tm.tradetracker.net/conversion?s=' + encodeURIComponent(campaignID) + '&t=m';
                                    var s = document.getElementsByTagName('script'); s = s[s.length - 1]; s.parentNode.insertBefore(tt, s);
                                })(ttConversionOptions);
    
                                //2
                                ttConversionOptions.push({
                                    type: 'lead',
                                    campaignID: '26607',
                                    productID: '39126',
                                    transactionID: data.client_id,
                                    email: data.client_email,
                                    descrMerchant: '',
                                    descrAffiliate: ''
                                });
                                $('body').append('<img src="//tl.tradetracker.net/?cid=26607&amp;pid=39126&amp;tid='+data.client_id+'&amp;data=&amp;eml='+data.client_email+'&amp;descrMerchant=&amp;descrAffiliate=&amp;event=lead" alt="" />');
                                (function(ttConversionOptions) {
                                    var campaignID = 'campaignID' in ttConversionOptions ? ttConversionOptions.campaignID : ('length' in ttConversionOptions && ttConversionOptions.length ? ttConversionOptions[0].campaignID : null);
                                    var tt = document.createElement('script'); tt.type = 'text/javascript'; tt.async = true; tt.src = '//tm.tradetracker.net/conversion?s=' + encodeURIComponent(campaignID) + '&t=m';
                                    var s = document.getElementsByTagName('script'); s = s[s.length - 1]; s.parentNode.insertBefore(tt, s);
                                })(ttConversionOptions);

                                $(".steps .item").removeClass("active");
                                $(".steps .item:nth-child(2)").addClass("done"); 
                                $(".steps .item:nth-child(3)").addClass("active"); 
                                $('#contest-form')[0].reset();
                                openTab(6);
                                //gtm dataLayer
                                dataLayer.push({
                                    'u6': data.pdf,
                                    'event': 'summary'
                                });; 
                            }else{
                                var e_placement = $('#step3');
                                if( $('#final-error').length == 0 ){
                                    $(e_placement)
                                        .css('display', 'none')
                                        .append('<div id="final-error" class="error">'+data.errors.final+'</div>')
                                        .fadeIn();
                                }
                            }
                        },
                        error: function(error){
                            if(error){
                                var e_placement = $('#step3');
                                if( $('#final-error').length == 0 ){
                                    $(e_placement)
                                        .css('display', 'none')
                                        .append('<div id="final-error" class="error">Wystąpił błąd.</div>')
                                        .fadeIn();
                                }
                            }
                        }
                    });
                }
            }

        });

        // -- go prev step
        $('.go-prev-step').on('click', function(e){
            e.preventDefault();

            var current_step = parseInt($(this).parents(".tabcontent").children('div.current-step').html()),
                tab_id;

            tab_id = current_step - 1;

            openTab(tab_id);
            
            buildSlider()
        });


        // mobile detect
        var md = new MobileDetect(window.navigator.userAgent);
        if (md.mobile()) {
            $("body").addClass("mobile")
        }

        function mediaqueryresponse(mql){
            if (mql.matches){
                $('#fixed-box, #fixed-box-2, #fixed-box-3, #fixed-box-4').trigger('detach.ScrollToFixed');
            }
            else {
                var headerH = $("#fixed-box-4 h3").height()
                //fixed box on home page
                $('#fixed-box').scrollToFixed({ 
                    marginTop: 10, 
                    limit: $("#fixed-box-limit").offset().top - 533 - 200,
                    preFixed: function() { $(this).find('.feature-img-label').removeClass('unslide').addClass('slide') },
                    postFixed: function() { 
                        var fixedPos = $("#unfix").offset().top
                        if($(window).scrollTop() < (fixedPos + 10)){
                            $(this).find('.feature-img-label').removeClass('slide').addClass('unslide')
                        }
                    },
                });
                //fixed box on home page
                $('#fixed-box-2').scrollToFixed({ 
                    marginTop: 10, 
                    limit: $("#fixed-box-limit").offset().top - 533 - 200,
                });
                $('#fixed-box-3').scrollToFixed({ 
                    marginTop: 10, 
                    limit: $("#fixed-box-3-limit").offset().top - 533 - 185,
                });
                $('#fixed-box-4').scrollToFixed({ 
                    marginTop: 10, 
                    limit: $("#fixed-box-4-limit").offset().top - headerH - 122,
                }); 
            }
        }

        var mql = window.matchMedia("screen and (max-device-width: 991px)")
        $(window).load(function(){
            mediaqueryresponse(mql)
        })
        mql.addListener(mediaqueryresponse)


        $(function(){
            
            //event table scrollbar
            $('#events').perfectScrollbar();
            
            //go to next section
            $("#next-button").click(function(){
                var position = $("#page-content").offset().top
                $("html,body").animate({scrollTop: position}, 800)
            })   
            
            //open menu
            $("#btn-menu").click(function(e){
                e.preventDefault();
                $("body").addClass("menu-opened")
                $("#page-header").addClass("sliding")
            })
            
            //close menu
            $("#close-menu").click(function(e){
                e.preventDefault();
                $("body").removeClass("menu-opened")
            })
            
            /*$(".main-nav li:nth-child(2)").mouseenter(function(){
                $("body").removeClass("ambasadors").addClass("shop");
                
            })
            
            $(".main-nav li:nth-child(1)").mouseenter(function(){
                $("body").removeClass("shop").addClass("ambasadors");
                
            })*/
            
            $(".main-nav li:nth-child(2)").mouseleave(function(){
                $("body").removeClass("ambasadors")
            })
            
            $(".main-nav .menu li").click(function(){
                setTimeout(function(){
                    $("body").removeClass("menu-opened")
                },300)
            })
            
            var roundSlider,
                productDetails,
                roundSliderMobile;
            
            function buildSliders(){
                if (!(md.mobile())) {
                    roundSlider = $('#round-poducts-slider').flipster({
                        style: 'wheel',
                        spacing: 0.1,
                        scrollwheel: false,
                        start: 0,
                    })
                }else {
                    roundSliderMobile = $('#round-poducts-slider ul').slick({
                        arrows: false,
                        infinite: false,
                        initialSlide: 0,
                    })
                }
                productDetails = $("#product-details").slick({
                    arrows: true,
                    infinite: false,
                    initialSlide: 0,
                    prevArrow: '<i><img src="'+fr.theme_url+'/assets/img/svg/arrow_left.svg" alt=""></i>',
                    nextArrow: '<i><img src="'+fr.theme_url+'/assets/img/svg/arrow_right.svg" alt=""></i>',
                    fade: true,
                    responsive: [{

                        breakpoint: 1024,
                        settings: {
                        slidesToShow: 1,
                        infinite: true
                    }

                    }, {

                        breakpoint: 600,
                        settings: {
                        slidesToShow: 1,
                    }

                    }, {

                        breakpoint: 300,
                        settings: "unslick" // destroys slick

                    }]
                });
            }
            
            
            function loadSliders(aa){
                $("#stylization-sliders").load("/wp-content/themes/frogriot/assets/ajax-sliders/stylization-"+aa+".html", function(responseTxt, statusTxt, xhr){
                    if(statusTxt == "success") {
                        buildSliders()
                        // On before slide change
                        $('#product-details').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                            showProduct(nextSlide)
                        });

                        $('#round-poducts-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                            showProduct(nextSlide)
                        })

                        
                        $("#product-slider .item.slick-current .icons .icon-item").each(function(item){
                            $(this).click(function(){
                                showProduct(item)
                            })
                        })

                        $("#round-poducts-slider .flipster__item img").each(function(item){
                            $(this).click(function(){
                                showProduct(item)
                            })
                        })
                        
                        setTimeout(function(){
                            $("#sliders").addClass("show")
                        }, 300)
                    }
                        
                    //if(statusTxt == "error")
                        //alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            
            loadSliders('0')
            
            var datesSlider = $('#datesSlider').slick({
                dots: false,
                arrows: false,
                fade: true,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
            });
            
            /*var descSlider = $('#desc-slider').slick({
                dots: false,
                arrows: false,
                fade: true,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
            });*/
            
            
            function showProduct(item){
                $("#product-slider .item.slick-current .icons .icon-item").removeClass("active")

                productDetails.slick('slickGoTo', item, false);
                
                if (!(md.mobile())) {
                    roundSlider.flipster('jump', item);
                } else {
                    roundSliderMobile.slick('slickGoTo', item, false);
                }
                $("#product-slider .item.slick-current .icons .icon-item:nth-child("+(item + 1)+")").addClass("active")
            }

            $('#product-slider').on('init', function(event, slick, currentSlide, nextSlide){
                
                
                
                var timer = 0;
                $(this).find('.slick-active').find(".icons").find(".icon-item").each(function(index){
                    var item = $(this)
                    setTimeout(function(){
                        $(item).addClass("show")
                    },400 + timer)
                    timer = timer + 100
                })
                $("#product-slider .item.slick-current .icons .icon-item:nth-child(1)").addClass("active")
            });
            
            $("#product-slider").slick({
                arrows: false,
                infinite: false,
                dots: true,
                adaptiveHeight: true,
                speed: 800,
                customPaging: function(slider, i) {
                    return $('<div class="pag-button" type="button" data-role="none" role="button" tabindex="0"></div>').text(i + 1);
                },
                responsive: [{

                    breakpoint: 1024,
                    settings: {
                    slidesToShow: 1,
                    infinite: true,
                }

                }, {

                    breakpoint: 600,
                    settings: {
                    slidesToShow: 1,
                    dots: true
                }

                }, {

                    breakpoint: 300,
                    settings: "unslick" // destroys slick

                }]
            });
            
            function showDescription(item){
                datesSlider.slick('slickGoTo', item, false);
                //descSlider.slick('slickGoTo', item, false);
            }
            
            
            $('#product-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                
                $(this).find('.slick-slide').find(".icons").find(".icon-item").removeClass("active show").each(function(index){
                    var item = $(this)

                    var timerIcons = setTimeout(function(){
                        $(item).addClass("show")
                    },400 + (index*20))

                })
                
                $("#product-slider .item[data-slick-index="+nextSlide+"] .icons .icon-item:nth-child(1)").addClass("active")
                
                $("#stylization").remove();
                loadSliders(nextSlide);
                
                showDescription(nextSlide);
                if (nextSlide == 1 || nextSlide == 2 || nextSlide == 3) {
                    $("#hide-sliders").css("opacity", "0");
                }
                if (nextSlide == 0) {
                    $("#hide-sliders").css("opacity", "1");
                }
            });

            
            
            
            var timer1 = 100;

            
            function showFeatures(elem, timer){
                $("#" + elem).find(".features-list li").each(function(){
                    var that = $(this)
                    setTimeout(function(){
                        $(that).addClass("show")
                    },timer + 100)
                    timer = timer + 125
                })
            }
            
            new Waypoint({
                element: document.getElementById('waypoint-1'),
                handler: function(direction) {
                    $("#waypoint-1 .feature-img-container").addClass("show")
                    $("#waypoint-1 .feature-img-label").addClass("show")
                    $("#waypoint-1 .section-header").addClass("show")
                    setTimeout(function(){
                        $("#waypoint-1 .waypoint-1-intro").addClass("show")
                    }, 400)
                },
                offset: '60%'
            })
            
            
            new Waypoint({
                element: document.getElementById('waypoint-1-1'),
                handler: function(direction) {
                    showFeatures('waypoint-1-1', timer1)
                },
                offset: '60%'
            })
            
            
            new Waypoint({
                element: document.getElementById('waypoint-2'),
                handler: function(direction) {
                    showFeatures('waypoint-2', timer1)
                    $("#waypoint-2 .feature-img-container").addClass("show-reverse")
                },
                offset: '60%'
            })
            
            new Waypoint({
                element: document.getElementById('waypoint-3'),
                handler: function(direction) {
                    showFeatures('waypoint-3', timer1)
                    $("#waypoint-3 .feature-img-container").addClass("show")
                },
                offset: '60%'
            })
            
            new Waypoint({
                element: document.getElementById('waypoint-4'),
                handler: function(direction) {
                    showFeatures('waypoint-4', timer1)
                },
                offset: '60%'
            })
            
            var timer2 = 100;
            var timer3 = 100
            
            new Waypoint({
                element: document.getElementById('waypoint-5'),
                handler: function(direction) {
                    $("#waypoint-5 .pins .pin").each(function(){
                        var that = $(this)
                        setTimeout(function(){
                            $(that).addClass("show")
                        },timer2 + 100)
                        timer2 = timer2 + 100
                    })
                    $("#waypoint-5 .events-table-inner tr").each(function(){
                        var that = $(this)
                        setTimeout(function(){
                            $(that).addClass("show")
                        },timer3 + 100)
                        timer3 = timer3 + 100
                    })
                },
                offset: '70%'
            })
            
            if ($(window).width() < 992) {
                $("#product-slider .item .icons .icon-item").bind("click.scroll", function(){
                    var topPos = $(".round-poducts-slider").offset().top - 20
                    $("html, body").animate({scrollTop: topPos}, 300)
                })
            }
            
        });

    });


    // Create the loader and queue our 3 images. Images will not 
    // begin downloading until we tell the loader to start. 
    var loader = new PxLoader(), 
        backgroundImg = loader.addImage('url(img/img-main.jpg)');

    // callback that will be run once images are ready 
    loader.addCompletionListener(function() { 
        $(".preloader").addClass("loaded")
        $("body").addClass("loaded")
        $("#page-header").addClass("show")
        setTimeout(function(){
            $(".preloader").remove()
        },600)
        setTimeout(function(){
            $("#page-header").removeClass("init")
        },3600);
        var timerLoader = 100
        $(".page-header-content .anim-show").each(function(){
            var elem = $(this)
            setTimeout(function(){
                elem.addClass("show")
            }, 1000 + timerLoader)
            timerLoader = timerLoader + 300
        })
        
    }); 

    // begin downloading images 
    loader.start();
    
    // Add smooth scrolling to all links
      $("a").not("#ambasadorki-konkurs-link").on('click', function(event) {
          

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();

          // Store hash
          var hash = this.hash;

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });

}(jQuery));