// Create the loader and queue our 3 images. Images will not 
// begin downloading until we tell the loader to start. 
var loader = new PxLoader(), 
    backgroundImg = loader.addImage('url(img/img-main.jpg)');

// callback that will be run once images are ready 
loader.addCompletionListener(function() { 
    $(".preloader").addClass("loaded")
    $("body").addClass("loaded")
    $("#page-header").addClass("show")
    setTimeout(function(){
        $(".preloader").remove()
    },600)
    setTimeout(function(){
        $("#page-header").removeClass("init")
    },3600);
    var timerLoader = 100
    $(".page-header-content .anim-show").each(function(){
        var elem = $(this)
        setTimeout(function(){
            elem.addClass("show")
        }, 1000 + timerLoader)
        timerLoader = timerLoader + 300
    })
    
}); 

// begin downloading images 
loader.start();