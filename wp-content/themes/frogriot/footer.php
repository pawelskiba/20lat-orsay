<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>
        <footer id="page-footer">
            <div class="container grid grid__light">
                <div class="content">
                    <span class="copy">Wszelkie prawa zastrzeżone © Orsay 2017</span>
                    <div class="regulations">
                        <a href="<?php echo get_template_directory_uri(); ?>/regulations/Konkurs_20_lat_ORSAY_Regulamin.pdf" target="_blank" class="item">regulamin konkursu “20 lat ORSAY”</a>
                        <a href="<?php echo get_template_directory_uri(); ?>/regulations/Konkurs_20_lat_ORSAY-Ambasadorka_Regulamin.pdf" target="_blank" class="item">regulamin konkursu “AMBASADORKA ORSAY”</a>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>