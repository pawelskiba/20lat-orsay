<?php

if( isset($_GET['key']) ){

    $key = $_GET['key'];

    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php' );

    //check if applications exists
    global $wpdb;
    $table = $wpdb->prefix."contest_four";

    $result1 = $wpdb->prepare( "SELECT downloaded FROM $table WHERE  pdfkey = %s", $key);
    $result2 = $wpdb->get_col( $result1 );

    if( count($result1) != 0 && $result2[0] == 0 ){
        $filename = 'download/ebook.pdf'; // of course find the exact filename....        
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false); // required for certain browsers 
        header('Content-Type: application/pdf');

        header('Content-Disposition: attachment; filename="'. basename($filename) . '";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($filename));

        readfile($filename);

        $wpdb->update( 
            $table, 
            array(
                'downloaded'    => 1
            ),
            array(
                'pdfkey'    => $key
            )
        );

        exit;
    }else{
        wp_redirect('/');
        exit();
    }

}else{
    wp_redirect('/');
    exit();
}

?>