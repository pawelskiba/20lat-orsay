<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

if( @$_GET['pdf'] && @$_GET['ambasador'] ){
    $pdfkey = $_GET['pdf'];

    $ambasador = $_GET['ambasador'];

    global $wpdb;
    $table = $wpdb->prefix."contest_four";

    $results = $wpdb->get_results( $wpdb->prepare("SELECT * FROM $table WHERE pdfkey=%s LIMIT 1", "$pdfkey"), ARRAY_A );

    if( count($results) != 0){

        foreach ( $results as $result ){

            $confirmed = $result['confirmed'];

            if( $confirmed === '0' ){

                $wpdb->update(
                    $table, 
                    array(
                        'confirmed' => 1
                    ),
                    array(
                        'pdfkey' => $pdfkey
                    )
                );

                /*
                $fname = $result['fname'];
                $lname = $result['lname'];
                $email = $result['email'];
                $answer = $result['answer'];
                $img_src = $result['img_src'];
            
                $recipients = "20lat@orsay.com";
                $title = "Nowe zgłoszenie konkursowe";      
                $message = "<b>Imię: </b>".$fname."<br><br>"
                . "<b>Nazwisko: </b>".$lname."<br><br>"
                . "<b>Email: </b>".$email."<br><br>"
                . "<b>Odpowiedź: </b>".$answer."<br><br>"
                . "<b>Wybór:</b> ".$img_src;
                $headers = array('From:'.$fname. ' ' .$lname. ' - <'. $email . '>');
                wp_mail( $recipients, $title, $message, $headers);
                */
            }

        }

    }else{
        wp_redirect('/');
        exit;
    }

}
?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo bloginfo('name'); ?></title>
    <meta name="description" content="<?php if ( is_single() ) { single_post_title('', true); } else { bloginfo('description'); } ?>" />
    <meta name="author" content="Frogriot">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico" />
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-touch-icon.png">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i|Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i|Oswald:200,300,400,500,600,700&amp;subset=latin-ext" rel="stylesheet">
    <?php wp_head(); ?>
    
    <script>
        var dataLayer = [];
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WK5GSQ6');</script>
    <!-- End Google Tag Manager -->
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-70822751-13', 'auto');
      ga('send', 'pageview');

    </script>
    
    <meta property="og:locale" content="pl_PL">
    <meta property="og:type" content="article">
    <meta property="og:title" content="ORSAY POLSKA"> 
    <meta property="og:description" content="Świętuj z nami 20 lat Orsay w Polsce">
    <meta property="og:url" content="http://20lat.orsay.com/" />
    <meta property="og:image" content="http://20lat.frogriot.com/wp-content/themes/frogriot/assets/img/stage4/top_2.jpg">
    <meta property="og:site_name" content="ORSAY POLSKA">
</head>
<body>
    <script type='text/javascript'>
    var src = (('https:' == document.location.protocol) ? 'https://' : 'http://');
    new Image().src = src+'adsearch.adkontekst.pl/deimos/tracking/?tid=34359741893&reid=AKCS3525&expire=5&nc=1503573672222666775518';
    </script>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WK5GSQ6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        
    <script>
    window._adftrack = Array.isArray(window._adftrack) ? window._adftrack : (window._adftrack ? [window._adftrack] : []);
    (function () {
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://track.adform.net/serving/scripts/trackpoint/async/';
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();

    var ttConversionOptions = ttConversionOptions || [];
    </script>
    
<header id="page-header" class="init">
    <div class="bg-image"></div>
    <div class="preloader">
        <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
        </div>
    </div>
    <a href="#" id="down-btn" class="down-btn"></a>
    <div class="content">
        <div class="container grid grid__light">
            <div class="page-header-head">
                <a href="/" class="logo-header">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/20lat_logo.svg" alt="">
                </a>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/logo_orsay.svg" alt="" class="logo-orsay">
                <a href="#" id="btn-menu" class="btn-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <div class="page-header-content">
                <h1 class="anim-show">
                    Świętuj z nami<br>20 lat ORSAY<br class="mobile-br"> w Polsce!
                </h1>
                <div class="desc anim-show">
                    Dołącz do<br class="mobile-br"> Klubu Modnych Kobiet<br> i zostań naszą Ambasadorką, jak
                </div>
                <div class="name anim-show">Ania Starmach</div>
                <div class="cta-wrapper anim-show">
                    <a href="#page-content" class="btn btn__black">wybory ani starmach</a>
                    <a href="#ambasadors-section" class="btn btn__black">zostań ambasadorką</a>
                </div>
            </div>
            <div class="next-button" id="next-button">
                <div class="arrow bounce"></div>
            </div>
        </div>
    </div>
    <nav class="main-nav">
        <div id="close-menu" class="close-menu"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/close.svg" alt=""></div>
        <ul class="menu">
            <li>
                <a href="http://www.orsay.com/pl-pl/?nsctrid=5555555117&utm_source=contest&utm_medium=referral&utm_campaign=20-years-pl&utm_content=mainpage" target="_blank">sklep online</a>
            </li>
            <li>
                <a class="hash" href="#ambasadors-section">konkurs</a>
            </li>
            <li>
                <a href="#waypoint-3">klub orsay</a>
            </li>
        </ul>
        <div class="notes">
            <a href="<?php echo get_template_directory_uri(); ?>/regulations/Konkurs_20_lat_ORSAY_Regulamin.pdf" target="_blank" class="item">Regulamin konkursu "20 lat orsay"</a>
            <a href="<?php echo get_template_directory_uri(); ?>/regulations/Konkurs_20_lat_ORSAY-Ambasadorka_Regulamin.pdf" target="_blank" class="item">Regulamin konkursu "Ambasadorka orsay"</a>
        </div>
    </nav>
</header>  