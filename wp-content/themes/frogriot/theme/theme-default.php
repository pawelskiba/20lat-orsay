<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Theme_Default' ) ) :

	class Theme_Default {

		function __construct() {

			$this->remove_junk();
			add_action('init', array($this, 'deregister_wpembed'));

			$this->menus();

			add_action( 'init', array($this, 'blockusers_init'));

			add_action('after_setup_theme', array($this, 'remove_admin_bar'));

			/*--------------------------------------------------------------------------------------------
			### ADD THEME IMAGES SIZE ###
			--------------------------------------------------------------------------------------------*/
			add_image_size( 'slider', 1920, 1024, array( 'center', 'center' ) );
			add_image_size( 'post-thumb', 379, 379, array( 'center', 'center' ) );
			
			add_filter( 'remove_orphans', array($this, 'remove_orphans') );
			add_filter( 'remove_orphans_p', array($this, 'remove_orphans_p') );

			add_filter( 'wp_mail_content_type', array($this, 'html_mails') );

		}

		/*--------------------------------------------------------------------------------------------
		### HTML EMAIL ###
		--------------------------------------------------------------------------------------------*/
		function html_mails(){
			return "text/html";
		}

		/*--------------------------------------------------------------------------------------------
		### REMOVE JUNK ###
		--------------------------------------------------------------------------------------------*/
		private function remove_junk() {
			remove_action( 'wp_head', 'rsd_link' ); // remove really simple discovery link
			remove_action( 'wp_head', 'wp_generator' ); // remove wordpress version

			remove_action( 'wp_head', 'feed_links', 2 ); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
			remove_action( 'wp_head', 'feed_links_extra', 3 ); // removes all extra rss feed links

			remove_action( 'wp_head', 'index_rel_link' ); // remove link to index page
			remove_action( 'wp_head', 'wlwmanifest_link' ); // remove wlwmanifest.xml (needed to support windows live writer)

			remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // remove random post link
			remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // remove parent post link
			remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // remove the next and previous post links
			remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

			remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
		}

		/*--------------------------------------------------------------------------------------------
		### DEREGISTER WP-EMBED ###
		--------------------------------------------------------------------------------------------*/
		function deregister_wpembed() {
			if (!is_admin()) {
				wp_deregister_script('wp-embed');
			}
		}
		
		/*--------------------------------------------------------------------------------------------
		### REGISTER MAIN THEME MENU ###
		--------------------------------------------------------------------------------------------*/
		private function menus() {
			register_nav_menus( array(
				'primary'   => 'Main Menu',
			) );
		}

		/*--------------------------------------------------------------------------------------------
		### PREVENT USER FROM ACCESSING WP-ADMIN ###
		--------------------------------------------------------------------------------------------*/
		function blockusers_init() {
			if ( is_admin() && ! current_user_can( 'administrator' ) && 
			! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				wp_redirect( home_url() );
				exit;
			}
		}

		/*--------------------------------------------------------------------------------------------
		### REMOVE ADMIN BAR FOR NON ADMIN USERS ###
		--------------------------------------------------------------------------------------------*/
		function remove_admin_bar() {
			if (!current_user_can('administrator') && !is_admin()) {
				show_admin_bar(false);
			}
		}
		
		
		/*--------------------------------------------------------------------------------------------
		### CUSTOM FILTERS ###
		--------------------------------------------------------------------------------------------*/
		//remove orphans and p tags, example: $text = apply_filters('remove_orphans', $text);
		function remove_orphans_p($content) {
			$content = strip_tags($content, '<br><b><i><a><quote><img><ul><li><ol><strong><code><span>');
			$remove = array(' w ',' z ',' o ',' i ',' a ', ' W ',' Z ',' O ',' I ',' A ');
			$insert = array(' w&nbsp;',' z&nbsp;',' o&nbsp;',' i&nbsp;',' a&nbsp;', ' W&nbsp;',' Z&nbsp;',' O&nbsp;',' I&nbsp;',' A&nbsp;');
			$content = str_replace($remove, $insert, $content);
			return $content;
		}

		/*--------------------------------------------------------------------------------------------
		### CUSTOM FILTERS ###
		--------------------------------------------------------------------------------------------*/
		//remove orphans and p tags, example: $text = apply_filters('remove_orphans', $text);
		function remove_orphans($content) {
			$remove = array(' w ',' z ',' o ',' i ',' a ', ' W ',' Z ',' O ',' I ',' A ');
			$insert = array(' w&nbsp;',' z&nbsp;',' o&nbsp;',' i&nbsp;',' a&nbsp;', ' W&nbsp;',' Z&nbsp;',' O&nbsp;',' I&nbsp;',' A&nbsp;');
			$content = str_replace($remove, $insert, $content);
			return $content;
		}

		/*--------------------------------------------------------------------------------------------
		### PAGINATION FUNCTION ###
		--------------------------------------------------------------------------------------------*/
		function custom_pagination($max_num_pages) {
			global $wp_query;
			$big = 123456789;
			$page_format = paginate_links( array(
				'base'         => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'current'      => max( 1, get_query_var('paged') ),
				'format'       => '?paged=%#%',
				'total'        => $max_num_pages,
				'show_all'     => false,
				'type'         => 'array',
				'prev_next'    => true,
				'prev_text'    => '',
				'next_text'    => '',
			) );
			if( is_array($page_format) ) {
				$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
				echo '<ul class="pagination">';
				foreach ( $page_format as $page ) {
					echo "<li class='pagination__item'>$page</li>";
				}
				echo '</ul>';
			}
		}

	}

endif;