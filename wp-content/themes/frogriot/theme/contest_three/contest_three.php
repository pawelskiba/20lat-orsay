<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

global $contest_three_db_version;
$contest_three_db_version = '1.0';

//import required class
require_once('contest_three-listtable.php');

/**
 * create db table on theme activation
 */
function contest_three_create_table() {
    global $wpdb;
	global $contest_three_db_version;

	$table_name = $wpdb->prefix . 'contest_three';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
        img_src mediumtext NULL,
        fname tinytext NOT NULL,
        lname tinytext NOT NULL,
        email VARCHAR(100) NOT NULL,
        answer text NOT NULL,
        club tinytext NOT NULL,
        ambasador tinytext NOT NULL,
        agree1 tinytext NOT NULL,
        agree2 tinytext NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'contest_three_db_version', $contest_three_db_version );
}
add_action('after_switch_theme', 'contest_three_create_table');

/**
 * admin_menu hook implementation
 */
function contest_three_admin_menu() {
    add_menu_page(__('Konkurs 3', 'orsay'), __('Konkurs 3', 'orsay'), 'activate_plugins', 'contest_three', 'contest_three_page_handler');
    add_submenu_page('contest_three', __('Konkurs 3', 'orsay'), __('Konkurs 3', 'orsay'), 'activate_plugins', 'contest_three', 'contest_three_page_handler');
    add_submenu_page('contest_three', __('Dodaj nowy', 'orsay'), __('Dodaj nowy', 'orsay'), 'activate_plugins', 'contest_three_form', 'contest_three_form_page_handler');
}
add_action('admin_menu', 'contest_three_admin_menu');

/**
 * Render table, applications page handler
 */
function contest_three_page_handler() {
    global $wpdb;

    $table = new contest_three_List_Table();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Usunięte: %d', 'orsay'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">

        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2>
        <?php _e('Konkurs 3', 'orsay')?>
        <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=contest_three_form');?>"><?php _e('Dodaj nowy', 'orsay')?></a>
        </h2>
        
        <?php echo $message; ?>

        <form id="applications-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>

    </div>
<?php
}

/**
 * Form page handler checks is there some data posted and tries to save it
 * Also it renders basic wrapper in which we are callin meta box render
 */
function contest_three_form_page_handler() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'contest_three'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'img_src' => '',
        'fname' => '',
        'lname' => '',
        'email' => '',
        'answer' => '',
        'club' => '',
        'ambasador' => '',
        'agree1' => '',
        'agree2' => '',
    );

    // here we are verifying does this request is post back and have correct nonce
    if ( isset($_REQUEST['nonce']) && wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);
        // validate data, and if all ok save item to database
        // if id is zero insert otherwise update
        $item_valid = validate_contest_three($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Zapisano', 'orsay');
                } else {
                    $notice = __('Wystąpił błąd', 'orsay');
                }
            } else {
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                if ($result) {
                    $message = __('Zaktualizowano', 'orsay');
                } else {
                    $notice = __('Wystąpił błąd', 'orsay');
                }
            }
        } else {
            // if $item_valid not true it contains error message(s)
            $notice = $item_valid;
        }
    }
    else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Nie znaleziono', 'orsay');
            }
        }
    }

    // add custom metabox
    add_meta_box('contest_three_form_meta_box', 'Dane', 'contest_three_form_meta_box_handler', 'contest_three', 'normal', 'default');

    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Konkurs 3', 'orsay')?> <a class="add-new-h2"
                                    href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=contest_three');?>"><?php _e('Powrót do listy', 'orsay')?></a>
        </h2>

        <?php if (!empty($notice)): ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
        <?php endif;?>
        <?php if (!empty($message)): ?>
        <div id="message" class="updated"><p><?php echo $message ?></p></div>
        <?php endif;?>

        <form id="form" method="POST">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
            <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
            <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

            <div class="metabox-holder" id="poststuff">
                <div id="post-body">
                    <div id="post-body-content">
                        <?php /* And here we call our custom meta box */ ?>
                        <?php do_meta_boxes('contest_three', 'normal', $item); ?>
                        <input type="submit" value="<?php _e('Zapisz', 'orsay')?>" id="submit" class="button-primary" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php
}

/**
 * Render meta box
 * $item is row
 *
 * @param $item
 */
function contest_three_form_meta_box_handler($item) { ?>

<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="img_src"><?php _e('Adres url obrazka', 'orsay')?></label>
        </th>
        <td>
            <input id="img_src" name="img_src" type="text" style="width: 95%" value="<?php echo esc_attr($item['img_src'])?>"
                   size="50" class="code" placeholder="<?php _e('Adres url obrazka', 'orsay')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="fname"><?php _e('Imię', 'orsay')?></label>
        </th>
        <td>
            <input id="fname" name="fname" type="text" style="width: 95%" value="<?php echo esc_attr($item['fname'])?>"
                   size="50" class="code" placeholder="<?php _e('Imię', 'orsay')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="lname"><?php _e('Nazwisko', 'orsay')?></label>
        </th>
        <td>
            <input id="lname" name="lname" type="text" style="width: 95%" value="<?php echo esc_attr($item['lname'])?>"
                   size="50" class="code" placeholder="<?php _e('Nazwisko', 'orsay')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="email"><?php _e('E-Mail', 'orsay')?></label>
        </th>
        <td>
            <input id="email" name="email" type="email" style="width: 95%" value="<?php echo esc_attr($item['email'])?>"
                   size="50" class="code" placeholder="<?php _e('Adres e-mail', 'orsay')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="answer"><?php _e('Odpowiedź', 'orsay')?></label>
        </th>
        <td>
            <textarea id="answer"
					  name="answer"
					  style="width: 95%"
					  rows="6" cols="50"
					  class="code"
					  placeholder="<?php _e('Odpowiedź', 'answer')?>"><?php echo esc_attr($item['answer'])?></textarea>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="club"><?php _e('Numer karty', 'orsay')?></label>
        </th>
        <td>
            <input id="club" name="club" type="club" style="width: 95%" value="<?php echo esc_attr($item['club'])?>"
                   size="50" class="code" placeholder="<?php _e('Numer karty', 'orsay')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="ambasador"><?php _e('Ambasador', 'orsay')?></label>
        </th>
        <td>
            <select id="ambasador" name="ambasador" style="width: 95%">
                <option selected value="TAK"><?php _e('TAK', 'orsay')?></option>
                <option value="NIE"><?php _e('NIE', 'orsay')?></option>
            </select>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="agree1"><?php _e('Zgoda 1', 'orsay')?></label>
        </th>
        <td>
            <select id="agree1" name="agree1" style="width: 95%">
                <option selected value="TAK"><?php _e('TAK', 'orsay')?></option>
                <option value="NIE"><?php _e('NIE', 'orsay')?></option>
            </select>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="agree2"><?php _e('Zgoda 2', 'orsay')?></label>
        </th>
        <td>
            <select id="agree2" name="agree2" style="width: 95%">
                <option selected value="TAK"><?php _e('TAK', 'orsay')?></option>
                <option value="NIE"><?php _e('NIE', 'orsay')?></option>
            </select>
        </td>
    </tr>

    </tbody>
</table>
<?php
}

/**
 * Simple function that validates data and retrieve bool on success
 * and error message(s) on error
 *
 * @param $item
 * @return bool|string
 */
function validate_contest_three($item) {
    $messages = array();

    if (!empty($item['email']) && !is_email($item['email'])) $messages[] = __('Nieprawidłowy adres email', 'orsay');

    if (empty($messages)) return true;
    return implode('<br />', $messages);
}
?>
