<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! class_exists( 'Theme_Ajax' ) ) :

class Theme_Ajax{

    function __construct(){

		//ajax process_contest_form
        add_action( 'wp_ajax_process_contest_form', array($this, 'process_contest_form') );
        add_action( 'wp_ajax_nopriv_process_contest_form', array($this, 'process_contest_form') );
    }

    function process_contest_form() {

        $errors = array();
        $passed = false;
        $pdfkey = '';
        $id = 0;
        $email = '';

        //security check - start
        if( check_ajax_referer( 'or_nonce', 'security' ) == true ){

            $img_src = esc_url($_POST['img_src']);

            $fname  = sanitize_text_field($_POST['fname']);
            $lname  = sanitize_text_field($_POST['lname']);
            $answer = sanitize_text_field($_POST['answer']);
            $club   = sanitize_text_field($_POST['club']);

            $email  = $_POST['email'];

            $check2 = $_POST['check2'];
            $check3 = $_POST['check3'];
            $check4 = $_POST['check4'];

            //email validation
            if ( ! $fname || ! $lname || ! $answer || ! $img_src ) {
                $errors['final'] = 'Wystapił błąd.';
            }

            //check2 validation
            if( isset( $check2 ) && $check2 == 1 ) {
                $check2 = 'TAK';
            }else{
                $check2 = 'NIE';
            }

            //check3 validation
            if( isset( $check3 ) && $check3 == 1 ) {
                $check3 = 'TAK';
            }else{
                $check3 = 'NIE';
            }

            //check4 validation
            if( isset( $check4 ) && $check4 == 1 ) {
                $check4 = 'TAK';
            }else{
                $check4 = 'NIE';
            }

            //email validation
            if ( ! $email ) {
                $errors['email'] = 'Pole obowiązkowe.';
            }else if ( ! is_email( $email ) ) {
                $errors['email'] = 'Niepoprawny adres e-mail!';
            }else{
                //sanitize email
                $email  = sanitize_email($email);
            }

            global $wpdb;
            $table = $wpdb->prefix."contest_four";

            $result = $wpdb->get_results("SELECT * FROM $table WHERE email = '".$email."'");

            if( count($result) > 0 ){
                $errors['final'] = 'Twoje zgłoszenie znajduje się już w naszej bazie.';
            }

            //save in db if no errors
            if( empty( $errors ) ){

                $pdfkey = md5(microtime());

                $wpdb->insert( 
                    $table, 
                    array(
                        'img_src' => $img_src,
                        'fname' => $fname,
                        'lname' => $lname,
                        'email' => $email,
                        'answer' => $answer,
                        'club' => $club,
                        'ambasador' => $check2,
                        'agree1' =>  $check3,
                        'agree2' =>  $check4,
                        'pdfkey' => $pdfkey,
                    )
                );
                
                $passed = true;

                $recipients = $email;
                $recipients2 = "konkurs@20latorsay.pl";

                $title = "Potwierdzenie zgłoszenia konkursowego Orsay";
                        
                //$message = "<b>Potwierdzenie: </b>". home_url().'/'."?pdf=$pdfkey&ambasador=$check2";
                //$message = file_get_contents(get_template_directory_uri().'/html/thx.html');

                $message = '';
                $message .= '<!DOCTYPE html><html><head><meta charset="utf-8">';
                $message .= '
                <style>
                @font-face {
                  font-family: \'Nunito Sans\';
                  font-style: normal;
                  font-weight: 400;
                  src: local(\'Nunito Sans Regular\'), local(\'NunitoSans-Regular\'), url(https://fonts.gstatic.com/s/nunitosans/v2/iJ4p9wO0GDKJ-D5teKuZqiYE0-AqJ3nfInTTiDXDjU4.woff2) format(\'woff2\');
                  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
                }
                @font-face {
                  font-family: \'Nunito Sans\';
                  font-style: normal;
                  font-weight: 400;
                  src: local(\'Nunito Sans Regular\'), local(\'NunitoSans-Regular\'), url(https://fonts.gstatic.com/s/nunitosans/v2/iJ4p9wO0GDKJ-D5teKuZqo4P5ICox8Kq3LLUNMylGO4.woff2) format(\'woff2\');
                  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
                }
                @font-face {
                  font-family: \'Oswald\';
                  font-style: normal;
                  font-weight: 500;
                  src: local(\'Oswald Medium\'), local(\'Oswald-Medium\'), url(https://fonts.gstatic.com/s/oswald/v14/cgaIrkaP9Empe8_PwXbajBJtnKITppOI_IvcXXDNrsc.woff2) format(\'woff2\');
                  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
                }
                @font-face {
                  font-family: \'Oswald\';
                  font-style: normal;
                  font-weight: 500;
                  src: local(\'Oswald Medium\'), local(\'Oswald-Medium\'), url(https://fonts.gstatic.com/s/oswald/v14/KuTkTNzljLi-9-e4QiI83ltXRa8TVwTICgirnJhmVJw.woff2) format(\'woff2\');
                  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
                }
                </style> ';
                $message .= '</head><body style="padding: 0; margin: 0;">';
                //content
                $message .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 15px">';
                //inner table
                $message .= '<tr>
                <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="570">';
                //logo
                $message .= '<tr>
                    <td align="center">
                        <img src="http://20lat.frogriot.com/wp-content/themes/frogriot/assets/img/logo_orsay.png" alt="Logo Orsay" border="0" style="display:block; margin: 25px auto 0;">
                    </td>
                </tr>';
                //text
                $message .= '<tr>
                    <td>
                        <p style="font-size: 20px; line-height: 1.5; font-family: \'Nunito Sans\', \'Tahoma\', sans-serif; margin-top: 25px; margin-bottom: 0">
                            Witaj,<br/> otrzymałaś tę wiadomość ponieważ Twój adres e-mail został podany przy zgłoszeniu
                            do konkursu "20 lat ORSAY". Prosimy <br/>o potwierdzenie udziału przez kliknięcie w poniższy
                            link.
                        </p>
                    </td>
                </tr>';
                //button
                $message .= '<tr>
                    <td align="center">
                        <div>
                            <!--[if mso]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:40px;v-text-anchor:middle;width:230px;" stroke="f" fillcolor="#f1ddd6">
                        <w:anchorlock/>
                        <center>
                        <![endif]-->
                            <a href="'.home_url().'/?pdf='.$pdfkey.'&ambasador='.$check2.'" style="margin:25px 0;background-color:#f1ddd6;color:#000000;display:inline-block;font-family: \'Oswald\', \'Tahoma\', sans-serif;font-size:15px;font-weight:bold;line-height:51px;text-align:center;text-decoration:none;width:250px;-webkit-text-size-adjust:none;">POTWIERDŹ ZGŁOSZENIE</a>
                            <!--[if mso]>
                        </center>
                        </v:rect>
                        <![endif]-->
                        </div>
                    </td>
                </tr>';
                //regards
                $message .= '<tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div class="header" style="margin-bottom:25px; font-size: 14px; font-family: \'Oswald\', \'Tahoma\', sans-serif;">
                                        Pozdrawiamy,<br/> Zespół Orsay
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>';
                $message .= '</table>
                    </td>
                </tr>';
                $message .= '</table>';
                //end of content
                $message .= '</body></html>';

                $headers = array('From: 20lat.orsay.com <20lat@orsay.com>');
                
                // Notification
                wp_mail( $recipients, $title, $message, $headers);
                            
                $message2 = "Mail z potwierdzeniem został wysłany do '.$email.'";
                // Notification
                wp_mail( $recipients2, $title, $message2, $headers);

                //get id
                $result2 = $wpdb->prepare( "SELECT id FROM $table WHERE  email = %s", $email);
                $result3 = $wpdb->get_col( $result2 );
                $id = $result3[0];
            }

        }//security check - end

        //response
        $response = array(
            'errors' => $errors,
            'passed' => $passed,
            'pdf' => $pdfkey,
            'client_email' => $email,
            'client_id' => $id
        );
        //send response
        wp_send_json($response);
    }

}

endif;
?>