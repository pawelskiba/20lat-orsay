<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Theme_Enqueue' ) ) :

	class Theme_Enqueue {
		
		function __construct() {
			add_action( 'wp_enqueue_scripts', [ $this, 'theme' ], 20 );
		}

		function theme() {
			if (!is_admin()) {
				wp_deregister_script('jquery');
			}

			//app.css
			wp_register_style( 'app-css', get_template_directory_uri() . '/dist/bundle.min.css', false, '1.0.12' );
			wp_enqueue_style( 'app-css' );
            

			//assets/js/modernizr.js
			wp_register_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.js', array(), '2.8.3' , true );
			wp_enqueue_script('modernizr');

			//assets/js/vendor.min.js
			wp_register_script( 'vendor', get_template_directory_uri() . '/assets/js/vendor.min.js', array(), '1.0.1' , true );
			wp_enqueue_script('vendor');
            
			//dist/bundle.min.js
			wp_enqueue_script( 'app-js', get_template_directory_uri() . '/dist/bundle.min.js', array(), '1.0.10', true );
			wp_localize_script( 'app-js', 'fr', array(
				'theme_url' => get_template_directory_uri(),
				'home_url' => esc_url( home_url( '/' ) ),
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'ajax_nonce' => wp_create_nonce('or_nonce'),
			) );
		}

	}

endif;