<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class contest_four_List_Table extends WP_List_Table {

	function __construct() {
        global $status, $page;

        parent::__construct(array(
            'singular' => 'contest_four',
            'plural' => 'contest_four',
        ));
    }

    /**
     * default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name) {
        if ($column_name == "url") {
            return '<a href="'.$item[$column_name].'" target="_blank">'.$item[$column_name].'</a>';
        }
        
        return $item[$column_name];
    }

    /**
     * render column with actions,
     * when you hover row "Edit | Delete" links showed
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_fname($item) {
        $actions = array(
            'edit' => sprintf('<a href="?page=contest_four_form&id=%s">%s</a>', $item['id'], __('Edytuj', 'orsay')),
            'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('usuń', 'orsay')),
        );

        return sprintf('%s %s',
            $item['fname'],
            $this->row_actions($actions)
        );
    }


    /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }

    /**
     * return columns to display in table
     *
     * @return array
     */
    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'img_src' => __('Adres url obrazka', 'orsay'),
            'fname' => __('Imię', 'orsay'),
            'lname' => __('Nazwisko', 'orsay'),
            'email' => __('Email', 'orsay'),
            'answer' => __('Odpowiedź', 'orsay'),
            'club' => __('Numer karty', 'orsay'),
            'ambasador' => __('Ambasador', 'orsay'),
            'agree1' => __('Zgoda 1', 'orsay'),
            'agree2' => __('Zgoda 2', 'orsay'),
            'pdfkey' => __('Klucz', 'orsay'),
            'confirmed' => __('Potwierdzenie', 'orsay'),
        );
        return $columns;
    }

	/**
     * @return array
     */
    function get_sortable_columns() {
        $sortable_columns = array(
            'img_src' => array('img_src', true),
            'fname' => array('fname', true),
            'lname' => array('lname', true),
            'email' => array('email', true),
            'answer' => array('answer', true),
            'club' => array('club', true),
            'ambasador' => array('ambasador', true),
            'agree1' => array('agree1', true),
            'agree2' => array('agree2', true),
        );
        return $sortable_columns;
    }

	/**
     * @return array
     */
    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Usuń'
        );
        return $actions;
    }

    function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'contest_four'; // do not forget about tables prefix

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

    /**
     * It will get rows from database and prepare them to be showed in table
     */
    function prepare_items() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'contest_four';

        $per_page = 25; // constant, how much records will be shown per page

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';

        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }

}
?>
