<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

//contest page in admin panel 1
require_once('theme/contest_one/contest_one.php');
//contest page in admin panel 2
require_once('theme/contest_two/contest_two.php');
//contest page in admin panel 3
require_once('theme/contest_three/contest_three.php');
//contest page in admin panel 4
require_once('theme/contest_four/contest_four.php');

spl_autoload_register( function ( $class ) {
	$classname_pieces = explode( '_', strtolower( $class ) );
	$class_file       = get_template_directory() . "/theme/" . implode( '-', $classname_pieces ) . ".php";
	if ( file_exists( $class_file ) ) {
		include_once  $class_file;
	} else {
		$class_file = get_template_directory() . "/theme/" . array_pop( $classname_pieces ) . "s/" . implode( '-', $classname_pieces ) . ".php";
		if ( file_exists( $class_file ) ) {
			include_once $class_file;
		}
	}
} );

$Theme_Enqueue = new Theme_Enqueue();
$Theme_Default = new Theme_Default();
$Theme_Ajax    = new Theme_Ajax();
?>